# pdfformat

CLI tool to modify PDF files from the scanner.

Can:
- Rotate page(s),
- Split page vertically (A3 to 2 A4),
- Remove pages
- Save parts
...